from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

urlpatterns = [
    path("admin/", admin.site.urls),
    path('receipts/', include('receipts.urls')),
    path('accounts/', include('accounts.urls')),
    path('home/', RedirectView.as_view(url='/receipts/home/'), name='home_redirect'),
]
