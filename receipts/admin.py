from django.contrib import admin
from .models import ExpenseCategory, Receipt, Account

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
        "id",
    ]

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "date",
        "category",
        "tax",
        "total",
        "purchaser",
        'account',
        "id"
    ]

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
        "id",
    ]
