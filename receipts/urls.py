from django.urls import path
from . import views

urlpatterns = [
    path('receipts/home/', views.receipt_list, name='home'),
    path('receipts/create/', views.create_receipt, name='create_receipt'),
    path('receipts/categories/', views.category_list, name='category_list'),
    path('accounts/', views.account_list, name='account_list'),
    path('accounts/create_category/', views.create_category, name='create_category'),
    path('accounts/create/', views.create_account, name='create_account'),
]
