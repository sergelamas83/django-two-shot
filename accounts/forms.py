from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Account

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)

class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "password", "first_name", "last_name"]
    
class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']
